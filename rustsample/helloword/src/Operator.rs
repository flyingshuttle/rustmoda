
use std::mem;

fn scope_and_shadowing(){
    let a = 123;

    {
        let a = 154;
        let b = 156;
        println!(" Inside  b = {}" , b )

         println!("The value a = {} ", a);
    }
    println!("The value a = {} ", a);
}

fn fundamental_data_operators{
    
    let mut a = 1+2*4;
    print!("a = {} ", a);
}

fn main(){

 scope_and_shadowing()
}