#![allow(dead_code)] //Disabling dead dead_code
 


pub fn matchstatement(){

    let countrycode = 65;

    let country = match countrycode {
        65 => "Singapore",
        91 => "India",
        1...99 => "unknown",  //1..99   1 98
        _ => "Invalid"
    };

    println!("Country {} ", country);
}