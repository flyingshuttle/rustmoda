#![allow(dead_code)]
#![allow(unused_variables)]

use std::thread;

pub fn threadexample()  {
    let xs = vec![1, 2, 3, 4, 5];

    //Spawns a thread
    //move(keyword) 
    // if move is removed, it will take reference the reference to the vector rather than the ownership of the vector.
    // move overrides the defaults. 
    let handle = thread::spawn(move || {
        println!("vector output  {:?}", xs);
    });

    handle.join().unwrap();
}