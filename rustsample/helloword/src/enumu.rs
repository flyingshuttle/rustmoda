#![allow(dead_code)]
#![allow(unused_variables)]
 

enum Color {  

    Red,
    Green,
    Blue,
    RgbColor(u8, u8, u8)  // tuple
                          // struct
 }

 
pub fn enums() {
 
 let c:Color = Color::RgbColor(10, 0, 0); 

 match c {
     Color::Red => println!("r"),
     Color::Blue => println!("b"),
     Color::Green => println!("g"),
     Color::RgbColor(0,0,0) => println!("Black"), 
     Color::RgbColor(r, g, b) => println!("rgb({}, {}, {})", r, g, b)
 }
}

 