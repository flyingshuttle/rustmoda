#![allow(dead_code)]
#![allow(unused_variables)]


pub fn option_function() {

//option<T>
    let x = 2.0;
    let y = 3.0;

    //let x = 0.0;
    //let y = 0.0;
     
    let result:Option<f64> =  if y != 0.0 { Some(x/y) } else {  None };

    println!("{:?}", result );


    match result {
        Some(z) => println!("{} {} = {}", x, y, z),
        None => println!("cannot divide {} by {} ", x, y) 
    }

    if let Some(z) = result { println!("z = {} ", z); }
}