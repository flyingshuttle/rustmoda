#![allow(dead_code)]
#![allow(unused_variables)]

//use std::mem;

pub fn vectors(){
    let mut a = Vec::new();
    a.push(1);
    a.push(2);
    a.push(3);
    println!("a = {:?}", a);
    a.push(33);
    println!("a = {:?}", a);
    // usize isize
    let idx:usize = 0;
     //print
    println!("a = {:?}", a[idx]);
}