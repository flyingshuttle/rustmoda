#![allow(dead_code)] //Disabling dead dead_code

use std::fmt;

struct Point{
    x: f64,
    y: f64,
}

  

impl fmt::Display for Point {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        // The `f` value implements the `Write` trait, which is what the
        // write! macro is expecting. Note that this formatting ignores the
        // various flags provided to format strings.
        write!(f, "({}, {})", self.x, self.y)
    }
}

pub fn struct_function(){

    let p1  = Point { x: 3.0, y: 4.0  };
    let p2  = Point { x: 10.0, y: 11.0  };
     //let l1 =  Line { start:p1, end:p2 };


    println!("The point P1 {} {} ", p1.x, p1.y);

    println!("The point P2 {}", p2);

    //println!("The point Line {:?}  ", l1);


}