#![allow(dead_code)] //Disabling dead dead_code


pub fn if_statement()
{
    let temp = 5;
    if temp > 30 {
        print!("Really hot outside");
    }else if temp < 10 {
        println!("Really cold");
    }else{
        println!("Temperature is ok");
    }
}   

pub fn for_loop() {
    for x in 1..10 {

        if x == 3 { continue; }

        if x == 8 { break; }

        println!( "The for loop {} ", x)
    }

    for ( pos, y) in (30..40).enumerate() {
        
        println!(" Position {}, {}", pos, y)

    }
}