#![allow(dead_code)] //Disabling dead dead_code

mod stackandheap;
mod ifstatements;
mod matcher;
mod structfunc;
mod enumu;
mod optionsio;
mod arraysmut;
mod vectorsd;
mod thread;

use std::mem;

const  MEANING_OF_LIFE: u8 = 42;

static mut T: u8 = 45;

fn scope_and_shadowing(){
    let a = 123;

    {
        let a = 154;
        let b = 156;
        println!(" Inside  b = {}" , b );

         println!("The value a = {} ", a);
    }
    println!("The value a = {} ", a);
}

fn fundamental_data_types() {
    
    let a:u8 = 123;
    //println!( "The value of a = {}" , a);
    println!("The value of a = {}, size  = {} bytes", a, mem::size_of_val(&a)  ) ; 
    
    let mut b:i8 = 124;
    println!( "The value of b = {}" , b);
    b = 42;
    println!("The value of b = {}", b);

    let c = 123456789;
    println!("The value of c = {}, size  = {} bytes", c, mem::size_of_val(&c)  ) ; 

    
    //i8 u8 i16 u16 i32 u32 i64 u64

    let z:isize = 123; // isize &  usize are integral data type which corresponds to the size of the word. 
    let size_of_z = mem::size_of_val(&z);
     
    println!("z= {}, takes up {} bytes, {}-bits OS", z, size_of_z, size_of_z * 8  );

    let d = 'x';
    println!("d = {}, takes up {} bytes ", d, mem::size_of_val(&d)  );

    let e:f32 = 2.50;
    println!("e = {}, takes up {} bytes ", e, mem::size_of_val(&e)  );

    let g = false;
    println!("g = {}, takes up {} bytes ", g, mem::size_of_val(&g)  );   
    let f = 4 > 0;
    
    println!("f = {}, takes up {} bytes ", f, mem::size_of_val(&f)  );
}

fn main() {
    //scope_and_shadowing();
    //fundamental_data_types();
    //println!( "{}", MEANING_OF_LIFE);
    //unsafe{
    //println!( "{}", T);
    //}
    //stackandheap::stack_and_heap();
    //ifstatements::for_loop();
    matcher::matchstatement();
    structfunc::struct_function();
    enumu::enums();
    optionsio::option_function();
    arraysmut::array_function(); 
    vectorsd::vectors();
    thread::threadexample();
}